<?php
  
namespace App\Http\Controllers;
   
use App\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
  
class ShortLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shortLinks = Url::latest()->get();
   
        return view('shortenLink', compact('shortLinks'));
    }
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $code = Str::lower(Str::random(3));

        $request->validate([
           'link' => 'required|url'
        ]);
   
        $url['link'] = $request->link;
        $url['shortlink'] = Str::lower(Str::random(6));

        $result = Url::where('shortlink', 'regexp', "^" . $url['link'] . $url['shortlink'] . "[0-9]?-[a-z0-9]{3}$")->orderBy('id',
            'desc')->first();

        if ($result) {
            preg_match("/" . preg_quote($url['link'], '/') . $url['shortlink'] . "(.*?)-/", $result->shortlink, $match);
            $increment = $match[1] ? ++$match[1] : 1;
            $input['link'] = $url['link'];
            $input['shortlink'] = $url['shortlink'] . $increment . "-" . $code;
        } else {
            $input['link'] = $url['link'];
            $input['shortlink'] = $url['shortlink'] . "-" . $code;
        }
        
        Url::create($input);

        return redirect('generate-shorten-link')
             ->with('success', 'Shorten Link Generated Successfully!');
    }
   
    /**
     * Redirect automatically to the link
     *
     * @return \Illuminate\Http\Response
     */
    public function shortenLink($shortlink)
    {
        $url = Url::where("shortlink", "like", "%$shortlink")->first();
        return redirect($url->shortlink);
    }
}