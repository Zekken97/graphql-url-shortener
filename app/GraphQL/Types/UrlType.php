<?php

namespace App\GraphQL\Types;

use App\Url;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UrlType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Url',
        'description' => 'Details about the link and the shortlink',
        'model' => Url::class
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'id of the url',
            ],
            'link' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'link of the url',
            ],
            'shortlink' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'shortlink of the url',
            ],
            'created' => [
                'type' => Type::Null(Type::string()),
                'description' => 'timestamp of created link',
            ],
            'modified' => [
                'type' => Type::Null(Type::string()),
                'description' => 'timestamp of updated link',
            ]
        ];
    }
}