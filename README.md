## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

# URL-SHORTENER GraphQL API
    git clone https://Zekken97@bitbucket.org/Zekken97/graphql-url-shortener.git
    cd url-shortner
    composer install
    cp .env.example .env
    php artisan serve
    php artisan test (to run the tests)

Url app: [http://localhost/graphql](http://localhost/graphql-playground)

Sample test: [http://localhost/url-shortener](http://localhost:8000/url-shortener)
